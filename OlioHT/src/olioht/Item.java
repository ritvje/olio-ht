/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Esineluokka (esineet säilötään Storage-luokassa)
 */
package olioht;

public class Item {
    private String name;
    private boolean fragile;
    private double size;
    private double mass;

    public String getName() {
        return name;
    }

    public boolean isFragile() {
        return fragile;
    }

    public double getSize() {
        return size;
    }

    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    Item (String inputName, boolean inputFragile, double inputSize, double inputMass){
        name = inputName;
        fragile = inputFragile;
        size = inputSize;
        mass = inputMass;
    }
    
}
