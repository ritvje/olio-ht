/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 *
 * Paketin lähetysluokan määräävien luokkien kantaluokka
 */
package olioht;

public class Class {
    
    protected String name;
    protected int number;
    protected double size;
    protected double mass;
    
    public double getSize() {
        return size;
    }

    public double getMass() {
        return mass;
    }

    public int getNumber() {
        return number;
    }
  
    public String getName() {
        return name;
    }
    
    public Class(){
        
    }   
}
