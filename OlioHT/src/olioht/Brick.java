/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Lähde: http://zetcode.com/tutorials/javagamestutorial/breakout/
 */
package olioht;

import javax.swing.ImageIcon;

public class Brick extends Sprite {

    private boolean destroyed;

    public Brick(int x, int y) {
        
        this.x = x;
        this.y = y;

        ImageIcon ii = new ImageIcon(getClass().getResource("tiili.png"));
        //ImageIcon ii = new ImageIcon(getClass().getResource("brickie.png"));
        image = ii.getImage();

        i_width = image.getWidth(null);
        i_heigth = image.getHeight(null);

        destroyed = false;
    }

    public boolean isDestroyed() {
        
        return destroyed;
    }

    public void setDestroyed(boolean val) {
        
        destroyed = val;
    }
}
