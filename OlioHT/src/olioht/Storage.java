/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Varastoluokka, joka pitää kirjaa paketeista ja esineistä
 */
package olioht;

import java.util.ArrayList;

public class Storage {
    
    static private Storage st = null;
    private ArrayList<Package> packages = null;
    private ArrayList<Item> items = null;

    public ArrayList<Item> getItems() {
        return items;
    }

    public ArrayList<Package> getPackages() {
        return packages;
    }
          
    static public Storage getInstance() {
        if (st == null) {
            st = new Storage();
        }
        return st;
    }
    
    public void addPackage(Package p) {
        packages.add(p);
    }
    
    public Package getPackage(String name) {
        for (Package p : packages) {
            if (p.getName().matches(name)) {
                return p;
            }
        }
        return null;
    }
    
    public Package modifyPackage(int id, String inputFromCity, String inputToCity, String inputFromAutomat, String inputToAutomat, int inputClassNumber) {
        for (Package p : packages) {
            if (p.getId() == id) {
                p.setFromCity(inputFromCity);
                p.setToCity(inputToCity);
                p.setFromAutomat(inputFromAutomat);
                p.setToAutomat(inputToAutomat);
                
                switch (inputClassNumber) {
                    case 1:
                        p.setTheClass(new FirstClass());
                        break;
                    case 2:
                        p.setTheClass(new SecondClass());
                        break;
                    case 3:
                        p.setTheClass(new ThirdClass());
                        break;
                }
                return p;
            }
        }
        return null;
    }
    
    public void addItem(Item item) {
        for (Item i : items) {
            if (i.getName().matches(item.getName())) {
                return;
            }
        }
        items.add(item);
    }
    
    public void removePackage(Package pkg) {
        for (Package p : packages) {
            if (p.getId() == pkg.getId()) {
                packages.remove(p);
                return;
            }
        }
    }
    
    public Item getItemByName(String name) {
        for (Item i : items) {
            if (i.getName().matches(name)) {
                return i;
            }
        }
        return null;
    }
    
    private Storage() {
        packages = new ArrayList();
        items = new ArrayList();
        
        Item pillow = new Item("Tyyny", false, 32000, 1.5);
        Item DWCF = new Item("Doctor Whooves -keräilyfiguuri",  false, 8000, 0.5);
        Item crystalBall = new Item("Kristallipallo", true,  27000, 3);
        Item tamagotchi = new Item("Tamagotchi", true, 125, 0.2);
        Item statueOfDavid = new Item("Daavidin patsas", true, 5170000, 5660);
        Item banana = new Item("Banaani", true, 500, 0.2);
        Item MLP = new Item("My little Pony", false, 500, 0.5);
        Item LN = new Item("Linux-nörtti", true, 225000, 49);
        Item cht = new Item("Chtulu", false, 517000000, 10000);
        
        items.add(pillow);
        items.add(DWCF);
        items.add(crystalBall);
        items.add(tamagotchi);
        items.add(statueOfDavid);  
        items.add(banana);
        items.add(MLP);
        items.add(LN);
        items.add(cht);
    }
}
