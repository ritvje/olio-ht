/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Ohjelman pääikkunaa kontrolloiva luokka
 */
package olioht;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button addAutomatButton;
    @FXML
    private ComboBox<String> SPautomatPicker;
    @FXML
    private Button CreatePackageButton;
    @FXML
    private Button UpdatePackageButton;
    @FXML
    private ComboBox<String> PackagePicker;
    @FXML
    private Button SendPackageButton;
    @FXML
    private Button RemoveRoutesButton;
    @FXML
    private Button DogeButton;
    @FXML
    private WebView webview;
    @FXML
    private Label titleField;
    @FXML
    private Label infoLabel;
    @FXML
    private Button logButton;
    
    private DataBuilder db = null;
    private Storage st = null;
    private LogWriter log = null;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Add map to webview
        webview.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        // Add Smartpost automats
        db = DataBuilder.getInstance();
        populateSmartPostComboBox();
        
        st = Storage.getInstance();  
        
        log = LogWriter.getInstance();
        
        loadLogIfUserWants();

    }    

    @FXML
    private void addAutomatButtonAction(ActionEvent event) {
        if (SPautomatPicker.getValue() != null) {
            webview.getEngine().executeScript("document.goToLocation(" + db.getScriptString(SPautomatPicker.getValue()) + ",'red')");
            db.addVisibleSmartpost(SPautomatPicker.getValue());
        }
    }

    @FXML
    private void CreatePackageButtonAction(ActionEvent event) {
        try {
            Stage PackageCreator = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageCreator.fxml"));
            Scene scene = new Scene(page);
            PackageCreator.setScene(scene);
            PackageCreator.show();
        
        } catch(IOException ex) {
            System.err.println("Can not open PackageCreator");
        }
    }

    @FXML
    private void UpdatePackageButtonAction(ActionEvent event) {
        try {
            Stage PackageUpdater = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageUpdater.fxml"));
            Scene scene = new Scene(page);
            PackageUpdater.setScene(scene);
            PackageUpdater.show();
        
        } catch(IOException ex) {
            System.err.println("Can not open PackageUpdater");
        }
    }

    @FXML
    private void SendPackageButtonAction(ActionEvent event) {
        infoLabel.setText(null);
        if (PackagePicker.getValue() != null) {
            Package p = st.getPackage(PackagePicker.getValue());
            String fromCoordinates = db.getSmartPost(p.getFromCity(), p.getFromAutomat()).getLocation().getPathCoordinates();
            String toCoordinates = db.getSmartPost(p.getToCity(), p.getToAutomat()).getLocation().getPathCoordinates();
            int classNumber = p.getClassNumber();
            
            // Send the package
            p.isSent();
            
            // Draw the route
            double d = (double) webview.getEngine().executeScript(
                    "document.createPath([" + fromCoordinates + "," + toCoordinates + "], 'blue', " + Integer.toString(classNumber) + ")");
            
            if ((classNumber == 1) && (d > 150)) {
                infoLabel.setText("Pakettia ei voitu toimittaa, sillä \nmatka on liian pitkä \n1. luokan paketille.");
                return;
            }
            else {
                infoLabel.setText("Paketti lähetetty!");
            }
            
            p.setDistance(d);
            
            // Write to log
            log.sendPackageLog(p);
            
            // Remove package from storage
            st.removePackage(p);
            
            PackagePicker.setValue(null);
            
        } else {
            infoLabel.setText("Valitse ensin paketti.");
        }
    }

    @FXML
    private void RemoveRoutesButtonAction(ActionEvent event) {
        webview.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void DogeButtonAction(ActionEvent event) {
        Breakout b = new Breakout();
        b.startGame();
    }

    @FXML
    private void removeAutomatsAction(ActionEvent event) {
        webview.getEngine().executeScript("document.deleteMarkers()");
        db.clearVisibleSmartposts();
    }

    @FXML
    private void showPackagesAction(Event event) {
        PackagePicker.getItems().clear();
        for (Package p : st.getPackages()) {
            PackagePicker.getItems().add(p.getName());
        }
    }
    
       @FXML
    private void openLogAction(ActionEvent event) {
        try {
            Stage Log = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLLogWindow.fxml"));
            Scene scene = new Scene(page);
            Log.setScene(scene);
            Log.show();
        
        } catch(IOException ex) {
            System.err.println("Can not open Log Window");
        }
    }
    
    private void populateSmartPostComboBox() {
        for (SmartPost sp : db.getSmartposts()) {
            SPautomatPicker.getItems().add(sp.getOfficialName());
        }
    }
 
    private void loadLogIfUserWants() {
        // Ask if user wants to load log from previous times
        // Alert requires JavaFX 8u40
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("");
        alert.setHeaderText("Ladataanko lähetetetyt ja tallennetut paketit?");
        alert.setContentText("Jos paketteja ei ladata, kaikki aikaisemmat\nhistoriatiedot tuhoutuvat.");
        
        ButtonType yesButton = new ButtonType("Lataa", ButtonData.OK_DONE);
        ButtonType noButton = new ButtonType("Älä lataa", ButtonData.CANCEL_CLOSE);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            LogWriter.loadSession();
        }
    }
}
