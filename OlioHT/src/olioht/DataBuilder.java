/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 *
 * Hakee pakettiautomaattien tiedot internetistä XML-muodossa ja parsii niistä 
 * SmartPost-olioita, jotka säilötään tässä luokassa
 */
package olioht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DataBuilder {
    
    private ArrayList<SmartPost> smartposts = null;
    private ArrayList<String> cities = null;
    private ArrayList<SmartPost> visibleSmartposts = null;
    private ArrayList<String> visibleCities = null;
    private Document doc; // to parse XML data
    static private DataBuilder db = null;
    
    public ArrayList<SmartPost> getVisibleSmartposts() {
        return visibleSmartposts;
    }

    public ArrayList<String> getVisibleCities() {
        return visibleCities;
    }
    
    public ArrayList<SmartPost> getSmartposts() {
        return smartposts;
    }
    
    public ArrayList<String> getCities() {
        return cities;
    }
   
    static public DataBuilder getInstance() {
        if (db == null) {
            db = new DataBuilder();
        }
        
        return db;
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    private void parseSmartPostData() {
        NodeList nodes = doc.getElementsByTagName("place");
        
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            smartposts.add(new SmartPost(getValue("postoffice", e).substring(19), getValue("availability", e), getValue("address", e), 
                    getValue("city", e), new Integer(getValue("code", e)), 
                    new GeoPoint(new Double(getValue("lat", e)), new Double(getValue("lng", e)))));
        }
    }
    
    public String getScriptString(String s) {
        for (SmartPost sp : smartposts) {
            if (sp.getOfficialName().equals(s)) {
                return sp.getScriptInfo();
            }
        }
        return null;
    }
    
    public ArrayList<SmartPost> getSmartPostsByCity(String city) {
        ArrayList<SmartPost> posts = new ArrayList();
        for (SmartPost sp : smartposts) {
            if (sp.getCity().matches(city)) {
                posts.add(sp);
            }
        }
        return posts;
    }
    
    public SmartPost getSmartPost(String city, String name) {
        for (SmartPost sp : smartposts) {
            if ((sp.getCity().matches(city)) && sp.getName().matches(name)) {
                return sp;
            }
        }
        return null;
    }
    
    public void addVisibleSmartpost(String name) {
        for (SmartPost sp : smartposts) {
            if (sp.getOfficialName().matches(name)) {
                visibleSmartposts.add(sp);
                if (!visibleCities.contains(sp.getCity())) {
                    visibleCities.add(sp.getCity());
                }
                return;
            }
        }
        return;
        
    }
    
    public void clearVisibleSmartposts() {
        visibleSmartposts.clear();
        visibleCities.clear();
        return;
    }
    
    public ArrayList<SmartPost> getVisibleSmartPostsByCity(String city) {
        ArrayList<SmartPost> posts = new ArrayList();
        for (SmartPost sp : visibleSmartposts) {
            if (sp.getCity().matches(city)) {
                posts.add(sp);
            }
        }
        return posts;
    }
    
    private DataBuilder() {
        smartposts = new ArrayList();
        cities = new ArrayList();
        visibleSmartposts = new ArrayList();
        visibleCities = new ArrayList();
        
        try {
            // Get SmartPost data from internet and parse XML
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

            String content = "";
            String line;

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
     
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            parseSmartPostData();
         
            // Sort SmartPosts by city name
            Collections.sort(smartposts, new Comparator<SmartPost>() {
                    @Override
                    public int compare(SmartPost sp1, SmartPost sp2) {
                        return  sp1.getCity().compareTo(sp2.getCity());
                    }
            });
            
            // Fill cities-ArrayList with city names
            
            cities.add(smartposts.get(0).getCity());
            int i = 0;
            for (SmartPost sp : smartposts) {
                if (!(sp.getCity().matches(cities.get(i)))) {
                    cities.add(sp.getCity());
                    i++;
                }
            }
           
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

