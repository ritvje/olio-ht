/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * 2. luokan pakettiluokka
 */
package olioht;

public class SecondClass extends Class {
    
    public SecondClass() {
        super();
        name = "2. luokka";
        number = 2;
        size = 100000f;
        mass = 500;
    }
}
