/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * 1. luokan pakettiluokka
 */
package olioht;

public class FirstClass extends Class {
    
    public FirstClass() {
        super();
        name = "1. luokka";
        number = 1;
        size = 27000f;
        mass = 3;
    }
    
}
