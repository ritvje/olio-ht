/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Pakettiautomaatti-luokka
 */
package olioht;

public class SmartPost {
    
    private final String availability;
    private final String name;
    private final String address;
    private final String city;
    private final int zipCode;
    private final GeoPoint location;

    public String getAvailability() {
        return availability;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
        
    public String getCity() {
        return city;
    }

    public int getZipCode() {
        return zipCode;
    }

    public GeoPoint getLocation() {
        return location;
    }
    
        // return the name with city to be shown on GUI list
    public String getOfficialName() {
        return this.city + ", " + this.name;
    }
    
    public String getScriptInfo() {
        return "'" + this.address + ", " + this.zipCode + " " + this.city + "', '" + this.name + " " + this.availability + "'";
    }
    
    public SmartPost(String n, String ava, String adrs, String c, int z, GeoPoint l) {
        availability = ava;
        name = n;
        address = adrs;
        city = c;
        zipCode = z;
        location = l;
    }
}

// Contains location info for SmartPost
class GeoPoint {
    
    private final double lat;
    private final double lon;

    public double getLatitude() {
        return lat;
    }

    public double getLongitude() {
        return lon;
    }
    
    public String getPathCoordinates() {
        return lat + "," + lon;
    }
    
    public GeoPoint(double la, double lo) {
        lat = la;
        lon = lo;
    }
}