/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Pakettiluokka (paketeista pidetään kirjaa Storage-luokassa)
 */
package olioht;

import java.util.Random;

public class Package {
    private Item theItem;
    private String fromCity;
    private String toCity;
    private String fromAutomat;
    private String toAutomat;
    private Class theClass;
    private boolean broken;
    private double distance = 0f;
    private final int id; 
    private static int counter = 1;
    
    public Class getTheClass() {
        return theClass;
    }
    public double getDistance() {
        return distance;
    }
    
    public boolean isBroken() {
        return broken;
    }
    
    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public void setFromAutomat(String fromAutomat) {
        this.fromAutomat = fromAutomat;
    }

    public void setToAutomat(String toAutomat) {
        this.toAutomat = toAutomat;
    }

    public void setTheClass(Class theClass) {
        this.theClass = theClass;
    }
    
    public void setIsBroken(boolean broken) {
        this.broken = broken;
    }

    public Item getTheItem() {
        return theItem;
    }

    public String getFromCity() {
        return fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public String getFromAutomat() {
        return fromAutomat;
    }

    public String getToAutomat() {
        return toAutomat;
    }
    
    public String getName() {
        return theItem.getName() + ", " + theClass.getName();
    }
    
    public int getClassNumber() {
        return theClass.getNumber();
    }
    
    public int getId() {
        return this.id;
    }
    
    public String toString() {
        return this.getTheItem().getName();
    }
    
    public void isSent() {
        if ((this.theClass.getNumber() == 1) && (this.getTheItem().isFragile())) {
            this.broken = true;
            return;
        }
        if ((this.theClass.getNumber() == 2) && (this.getTheClass().getSize() > 10000f) 
                && this.getTheItem().isFragile()) {
            this.broken = true;
            return;
        }
        if (this.theClass.getNumber() == 3) {
            this.broken = true;
            return;
        }
    }
    
    public Package(Item inputItem, String inputFromCity, String inputToCity, String inputFromAutomat, String inputToAutomat, int inputClassNumber){
        theItem = inputItem;
        fromCity = inputFromCity;
        toCity = inputToCity;
        fromAutomat = inputFromAutomat;
        toAutomat = inputToAutomat;
        
        switch (inputClassNumber) {
            case 1:
                theClass = new FirstClass();
                break;
            case 2:
                theClass = new SecondClass();
                break;
            case 3:
                theClass = new ThirdClass();
                break;
        }
        
        id = counter;
        counter++;
        
        broken = false;
    } 
}
