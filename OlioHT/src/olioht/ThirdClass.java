/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * 3. luokan pakettiluokka
 */
package olioht;

public class ThirdClass extends Class {
    
    public ThirdClass() {
        super();
        name = "3. luokka";
        number = 3;
        size = 100000;
        mass = 5000;
    }
    
}
