/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Pakettien luomiseen käytettävää ikkunaa kontrolloiva luokka
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.input.MouseEvent;


public class FXMLPackageCreatorController implements Initializable {
    @FXML
    private ComboBox<String> ItemPicker;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private CheckBox fragilityCheck;
    @FXML
    private ComboBox<String> fromCityPicker;
    @FXML
    private ComboBox<String> fromAutomatPicker;
    @FXML
    private ComboBox<String> toCityPicker;
    @FXML
    private ComboBox<String> toAutomatPicker;
    @FXML
    private Button makeButton;
    @FXML
    private RadioButton class1Choise;
    @FXML
    private RadioButton class2Choise;
    @FXML
    private RadioButton class3Choise;
    @FXML
    private Button infoButton;
    @FXML
    private Label infoLabel;
    @FXML
    private Button cancelButton;
    
    private boolean infoButtonHasBeenPressed = false;
    private DataBuilder db = null;
    private Storage st = null;
    private LogWriter log = null;
    private Item theItem;
    private int classNumber = 0;
    @FXML
    private Label titleText;
    @FXML
    private Separator subtitleText;


    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Add cities to comboboxes
        db = DataBuilder.getInstance();
        populateCityComboBoxes();
        
        // Initialize storage and log
        st = Storage.getInstance();
        populateItemBox();
        
        log = LogWriter.getInstance();
    }    

    @FXML
    private void makeButtonAction(ActionEvent event) {
        infoLabel.setText(null);
        
        // Check if classNumber is chosen
        if (class1Choise.isSelected()) {
            classNumber = 1;
        } else if (class2Choise.isSelected()) {
            classNumber = 2;
        } else if (class3Choise.isSelected()) {
            classNumber = 3;
        } else {
            infoLabel.setText("Valitse luokka!");
            classNumber = 0;
            return;
        }
        
        // Check that route info is chosen
        if ((fromCityPicker.getValue() == null) || (toCityPicker.getValue() == null) || (fromAutomatPicker.getValue() == null) || (toAutomatPicker.getValue() == null)){
            infoLabel.setText("Valitse reittitiedot!");
            return;
        }
        
        if ((!nameField.getText().trim().isEmpty()) || (!sizeField.getText().trim().isEmpty()) || (!massField.getText().trim().isEmpty())) {
            // Create a new Item
            if((!nameField.getText().isEmpty()) && (!sizeField.getText().isEmpty()) && (!massField.getText().isEmpty())) {
                double size = parseSize(sizeField.getText());
                if (size == 0) {
                    infoLabel.setText("Tarkasta koko.");
                    return;
                }
                theItem = new Item(nameField.getText(), fragilityCheck.isSelected(), size, Double.parseDouble(massField.getText()));
                st.addItem(theItem);
            } else {
                infoLabel.setText("Et voi tehdä omaa esinettä ilman kaikkia tietoja.");
                return;
            }
        } else {
            // Choose from existing Items
            if (ItemPicker.getValue() != null) {
                for (Item i : st.getItems()) {
                    if (i.getName().matches(ItemPicker.getValue())) {
                        theItem = i;
                        break;
                    }
                }
            } else {
                infoLabel.setText("Valitse esine.");
                return;
            }
        }
        
        if (theItem.getName().matches("Linux-nörtti")) {
            infoLabel.setText("Ammattiliitto ei salli vaarallisten tuotteiden kuljettamista.\n"
                    + "Valitse toinen esine.");
            return;
        }

        if (nameField.getText().equals("Doge")){
            infoLabel.setText("Laitoit sitten eläimen postiin menemään. Way to go.");
        } else if (nameField.getText().equalsIgnoreCase("Foobar") || nameField.getText().equalsIgnoreCase("Foo") || nameField.getText().equalsIgnoreCase("bar")){
            infoLabel.setText("Wow. Aika luovaa.");
        } else {
            infoLabel.setText("Paketti luotu!");
        }
      
        // Create the Package
        Package thePackage = new Package(theItem, fromCityPicker.getValue(), 
                toCityPicker.getValue(), fromAutomatPicker.getValue(), 
                toAutomatPicker.getValue(), classNumber);
        
        if ((theItem.getMass() > thePackage.getTheClass().getMass()) || (theItem.getSize() > thePackage.getTheClass().getSize())) {
            infoLabel.setText("Esine ei mahdu pakettiin.");
            st.removePackage(thePackage);
            return;
        }
        
        // Add Package to Storage
        st.addPackage(thePackage);
        infoLabel.setText("Paketti luotu!");
        classNumber = 0;
        
        // Write to log
        log.storePackageLog(thePackage);
   
        // close the window
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        // do whatever you want and close the window
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void infoButtonAction(ActionEvent event) {
        if (infoButtonHasBeenPressed == false){
            infoLabel.setText("1. luokka\nNopein pakettiluokka, mutta voi lähettää\nkorkeintaan 150 km päähän. Kaikki särkyvä\nsärkyy.\n\n"
                            + "2. luokka\nTurvakuljetus. Ei suurille paketeille.\n\n"
                            + "3. luokka\nHidas kuljetus. Jos työntekijämme ovat\nstressaantuneita, pakettia paiskotaan pitkin\nmäkiä. Hyvä suurille paketeille, sillä niitä\nei jaksa paiskoa niin paljon. Muutoin on\nparas toivoa, että Timolla on hyvä päivä.");
            infoButtonHasBeenPressed = true;
        }
        else{
            infoLabel.setText("");
            infoButtonHasBeenPressed = false;
        }
    }

    @FXML
    private void showAutomatsFrom(Event event) {
        fromAutomatPicker.getItems().clear();
        if (fromCityPicker.getValue() == null) {
            for (SmartPost sp : db.getVisibleSmartposts()) {
                fromAutomatPicker.getItems().add(sp.getOfficialName());
            }
        } else {
            for (SmartPost sp : db.getVisibleSmartPostsByCity(fromCityPicker.getValue())) {
                fromAutomatPicker.getItems().add(sp.getName());
            }
        }
    }

    @FXML
    private void showAutomatsTo(Event event) {
        toAutomatPicker.getItems().clear();
        if (toCityPicker.getValue() == null) {
            for (SmartPost sp : db.getVisibleSmartposts()) {
                toAutomatPicker.getItems().add(sp.getOfficialName());
            }
        } else {
            for (SmartPost sp : db.getVisibleSmartPostsByCity(toCityPicker.getValue())) {
                toAutomatPicker.getItems().add(sp.getName());
            }
        }
    }

    @FXML
    private void class1ClickedAction(MouseEvent event) {
        if (class2Choise.isSelected()){
            class2Choise.setSelected(false);
        }
        if (class3Choise.isSelected()){
            class3Choise.setSelected(false);
        }
        
    }

    @FXML
    private void class2ClickedAction(MouseEvent event) {
        if (class1Choise.isSelected()){
            class1Choise.setSelected(false);
        }
        if (class3Choise.isSelected()){
            class3Choise.setSelected(false);
        }
    }
    
    @FXML
    private void class3ClickedAction(MouseEvent event) {
        if (class1Choise.isSelected()){
            class1Choise.setSelected(false);
        }
        if (class2Choise.isSelected()){
            class2Choise.setSelected(false);
        }  
    }

    @FXML
    private void resetFromAutomatCombo(Event event) {
        fromAutomatPicker.getItems().clear();
        fromAutomatPicker.setValue(null);
    }

    @FXML
    private void resetToAutomatCombo(Event event) {
        toAutomatPicker.getItems().clear();
        toAutomatPicker.setValue(null);
    }

   private void populateCityComboBoxes() {
        for (String c : db.getVisibleCities()) {
            fromCityPicker.getItems().add(c);
            toCityPicker.getItems().add(c);
        }
    }
        
    private void populateItemBox() {
        for (Item i : st.getItems()) {
            ItemPicker.getItems().add(i.getName());
        }
    } 

    private double parseSize(String s) {
        try {
            String[] temp = s.split("\\*");
            return Double.parseDouble(temp[0])*Double.parseDouble(temp[1])*Double.parseDouble(temp[2]);
        } catch (IndexOutOfBoundsException e) {
            return 0f;
        }
    }
}
