/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Lokiin kirjoitettavista asioista kirjaa pitävä luokka
 */
package olioht;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LogWriter {
    
    static private LogWriter log = null;
    static private ObservableList<Package> sentData;
    static private ObservableList<Package> storedData;
    private static IntegerProperty sentPackages;
    private static IntegerProperty storedPackages;
    
    public static ObservableList<Package> getStoredData() {
        return storedData;
    }

    public static IntegerProperty getSentPackages() {
        return sentPackages;
    }

    public static IntegerProperty getStoredPackages() {
        return storedPackages;
    }

    public static ObservableList<Package> getSentData() {
        return sentData;
    }
    
    static public LogWriter getInstance() {
        if (log == null) {
            log = new LogWriter();
        }
        
        return log;
    }
    
    public void sendPackageLog(Package p) {
        sentData.add(p);
        sentPackages.set(sentData.size());
        removeStoredPackage(p);
        
    }
    
    public void storePackageLog(Package p) {
        storedData.add(p);
        storedPackages.set(storedData.size());
    }
    
    public void changePackageLog(Package pkg) {
        for (Package p : storedData) {
            if (p.getId() == pkg.getId()) {
                storedData.remove(p);
                storedData.add(pkg);
                return;
            }
        }
    }
    
    private void removeStoredPackage(Package pkg) {
        for (Package p : storedData) {
            if (p.getId() == pkg.getId()) {
                storedData.remove(p);
                storedPackages.set(storedData.size());
                return;
            }
        }
    }
    
    static public void saveSession() {
        try {
            BufferedWriter sentLog = new BufferedWriter(new FileWriter("sent-packages.txt"));
            BufferedWriter storedLog = new BufferedWriter(new FileWriter("stored-packages.txt"));
            BufferedWriter itemsLog = new BufferedWriter(new FileWriter("items.txt"));
            
            // Write sent packages to file
            sentLog.write("Lähetetyt paketit\n");
            
            for (Package p : sentData) {
                sentLog.write(p.getTheItem().toString() + "\t" + p.getFromAutomat()
                        + "\t" + p.getFromCity() + "\t" + p.getToAutomat() + "\t" 
                        + p.getToCity() + "\t" + p.getClassNumber() + "\t" +  
                        p.getDistance() + "\t" + p.isBroken() + "\n");
            }
            sentLog.close();
            
            // Write stored packages to file
            storedLog.write("Tallennetut paketit\n");
            
            for (Package p : storedData) {
                storedLog.write(p.getTheItem().toString() + "\t" + p.getFromAutomat()
                        + "\t" + p.getFromCity() + "\t" + p.getToAutomat() + "\t" 
                        + p.getToCity() + "\t" + p.getClassNumber() + "\n");
            }
            storedLog.close();
            
            // Save new Items
            itemsLog.write("Esineet\n");
            Storage st = Storage.getInstance();
            for (Item i : st.getItems()) {
                itemsLog.write(i.getName() + "\t" + i.isFragile() + "\t" + i.getSize() + "\t" + i.getMass() + "\n");
            }
            itemsLog.close();
            
            
        } catch (IOException ex) {
            Logger.getLogger(LogWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static public void loadSession() {
        try {
            Scanner loadSent = new Scanner(new FileReader("sent-packages.txt"));
            Scanner loadStored = new Scanner(new FileReader("stored-packages.txt"));
            Scanner loadItems = new Scanner(new FileReader("items.txt"));
            Storage st = Storage.getInstance();
            
            String splitStr = "\t";
            String str;
            String[] temp;
            Package tempPkg;
            
            // Load items from file
            str = loadItems.nextLine();
            while (loadItems.hasNextLine()) {
                str = loadItems.nextLine();
                temp = str.split(splitStr);
                st.addItem(new Item(temp[0], Boolean.getBoolean(temp[1]), 
                        Double.parseDouble(temp[2]), Double.parseDouble(temp[3])));
            }
            loadItems.close();
            
            // Read and save sent packages
            // First line is title -> don't save
            str = loadSent.nextLine();
            while (loadSent.hasNextLine()) {
                str = loadSent.nextLine();
                temp = str.split(splitStr);
                tempPkg = new Package(st.getItemByName(temp[0]), temp[2], temp[4], temp[1], temp[3], Integer.parseInt(temp[5]));
                tempPkg.setDistance(Double.parseDouble(temp[6]));
                tempPkg.setIsBroken(Boolean.parseBoolean(temp[7]));
                sentData.add(tempPkg);
            }
            loadSent.close();
            sentPackages.set(sentData.size());
            
            // Read and save stored packages
            // First line is title -> don't save
            str = loadStored.nextLine();
            while (loadStored.hasNextLine()) {
                str = loadStored.nextLine();
                temp = str.split(splitStr);
                tempPkg = new Package(st.getItemByName(temp[0]), temp[2], temp[4], temp[1], temp[3], Integer.parseInt(temp[5]));
                storedData.add(tempPkg);
                st.addPackage(tempPkg);
            }
            loadStored.close();
            storedPackages.set(storedData.size());
            

        } catch (FileNotFoundException ex) {
            Logger.getLogger(LogWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private LogWriter() {
        sentData = FXCollections.observableArrayList();
        storedData = FXCollections.observableArrayList();
        sentPackages = new SimpleIntegerProperty(0);
        storedPackages = new SimpleIntegerProperty(0);
    }
}
