/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Pakettien muokkaamiseen käytettävää ikkunaa kontrolloiva luokka
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class FXMLPackageUpdaterController implements Initializable {
    @FXML
    private ComboBox<Package> PackagePicker;
    @FXML
    private RadioButton class1Choice;
    @FXML
    private RadioButton class2Choice;
     @FXML
    private RadioButton class3Choice;
    @FXML
    private Button infoButton;
    @FXML
    private Label infoLabel;
    @FXML
    private ComboBox<String> fromCityPicker;
    @FXML
    private ComboBox<String> fromAutomatPicker;
    @FXML
    private ComboBox<String> toCityPicker;
    @FXML
    private ComboBox<String> toAutomatPicker;
    @FXML
    private Button cancelButton;
    @FXML
    private Button saveButton;

    private boolean infoButtonHasBeenPressed = false;
    private DataBuilder db = null;
    private Storage st = null;
    private LogWriter log = null;
    private int classNumber = 0;
    private Class c;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Add cities to comboboxes
        db = DataBuilder.getInstance();
        populateCityComboBoxes();
        
        // Initialize Storage and log
        st = Storage.getInstance();
        populatePackageComboBox();
        
        log = LogWriter.getInstance();
        
    }    

    @FXML
    private void choosePackageAction(Event event) {
        if (PackagePicker.getValue() != null) {
            Package p = PackagePicker.getValue();
            fromCityPicker.setValue(p.getFromCity());
            toCityPicker.setValue(p.getToCity());
            fromAutomatPicker.setValue(p.getFromAutomat());
            toAutomatPicker.setValue(p.getToAutomat());
            
            switch (p.getClassNumber()) {
                case 1:
                    class1Choice.setSelected(true);
                    class2Choice.setSelected(false);
                    class3Choice.setSelected(false);
                    classNumber = 1;
                    break;
                case 2:
                    class1Choice.setSelected(false);
                    class2Choice.setSelected(true);
                    class3Choice.setSelected(false);
                    classNumber = 2;
                    break;
                case 3:
                    class1Choice.setSelected(false);
                    class2Choice.setSelected(false);
                    class3Choice.setSelected(true);
                    classNumber = 3;
                    break;
            }
            
        }
    }

    @FXML
    private void resetFromAutomatCombo(Event event) {
        fromAutomatPicker.getItems().clear();
        fromAutomatPicker.setValue(null);
    }
    
    @FXML
    private void resetToAutomatCombo(Event event) {
        toAutomatPicker.getItems().clear();
        toAutomatPicker.setValue(null);
    }

    @FXML
    private void showAutomatsFrom(Event event) {
        fromAutomatPicker.getItems().clear();
        if (fromCityPicker.getValue() == null) {
            for (SmartPost sp : db.getVisibleSmartposts()) {
                fromAutomatPicker.getItems().add(sp.getOfficialName());
            }
        } else {
            for (SmartPost sp : db.getVisibleSmartPostsByCity(fromCityPicker.getValue())) {
                fromAutomatPicker.getItems().add(sp.getName());
            }
        }
    }

    @FXML
    private void showAutomatsTo(Event event) {
        toAutomatPicker.getItems().clear();
        if (toCityPicker.getValue() == null) {
            for (SmartPost sp : db.getVisibleSmartposts()) {
                toAutomatPicker.getItems().add(sp.getOfficialName());
            }
        } else {
            for (SmartPost sp : db.getVisibleSmartPostsByCity(toCityPicker.getValue())) {
                toAutomatPicker.getItems().add(sp.getName());
            }
        }
    }

    @FXML
    private void cancelButtonAction(ActionEvent event) {
        // do whatever you want and close the window
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void saveAction(ActionEvent event) {
        infoLabel.setText(null);
        if (PackagePicker.getValue() == null) {
            infoLabel.setText("Valitse paketti.");
            return;
        }
        if ((fromCityPicker.getValue() == null) || (toCityPicker.getValue() == null) || (fromAutomatPicker.getValue() == null) || (toAutomatPicker.getValue() == null)) {
            infoLabel.setText("Valitse ensin lähtö- ja kohdeautomaatit.");
            return;
        }
        
        if ((PackagePicker.getValue().getTheItem().getMass() > c.getMass()) 
                || (PackagePicker.getValue().getTheItem().getSize() > c.getSize())) {
            infoLabel.setText("Esine ei mahdu pakettiin.");
            return;
        }
        
        // Save the package
        Package newPackage = st.modifyPackage(PackagePicker.getValue().getId(), fromCityPicker.getValue(), 
                toCityPicker.getValue(), fromAutomatPicker.getValue(), 
                toAutomatPicker.getValue(), classNumber);
        
        // Write changes to log
        log.changePackageLog(newPackage);
        
        // Close window
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void infoButtonAction(ActionEvent event) {
        if (infoButtonHasBeenPressed == false){
            infoLabel.setText("1. luokka\nNopein pakettiluokka, mutta voi lähettää\nkorkeintaan 150 km päähän. Kaikki särkyvä\nsärkyy.\n\n"
                            + "2. luokka\nTurvakuljetus. Ei suurille paketeille.\n\n"
                            + "3. luokka\nHidas kuljetus. Jos työntekijämme ovat\nstressaantuneita, pakettia paiskotaan pitkin\nmäkiä. Hyvä suurille paketeille, sillä niitä\nei jaksa paiskoa niin paljon. Muutoin on\nparas toivoa, että Timolla on hyvä päivä.");
            infoButtonHasBeenPressed = true;
        }
        else{
            infoLabel.setText("");
            infoButtonHasBeenPressed = false;
        }
    }

    @FXML
    private void class1ClickedAction(MouseEvent event) {
        classNumber = 1;
        c = new FirstClass();
        if (class2Choice.isSelected()){
            class2Choice.setSelected(false);
        }
        if (class3Choice.isSelected()){
            class3Choice.setSelected(false);
        }
    }

    @FXML
    private void class2ClickedAction(MouseEvent event) {
        classNumber = 2;
        c = new SecondClass();
        if (class1Choice.isSelected()){
            class1Choice.setSelected(false);
        }
        if (class3Choice.isSelected()){
            class3Choice.setSelected(false);
        }
    }

    @FXML
    private void class3ClickedAction(MouseEvent event) {
        classNumber = 3;
        c = new ThirdClass();
        if (class1Choice.isSelected()){
            class1Choice.setSelected(false);
        }
        if (class2Choice.isSelected()){
            class2Choice.setSelected(false);
        } 
    }
    
    private void populateCityComboBoxes() {
        for (String c : db.getVisibleCities()) {
            fromCityPicker.getItems().add(c);
            toCityPicker.getItems().add(c);
        }
    }
    
    private void populatePackageComboBox() {
        for (Package p : st.getPackages()) {
            PackagePicker.getItems().add(p);
        }
    }
}
