/*
 * CT60A2410 Olio-ohjelmointi 
 * Harjoitustyö
 * Jenna Ritvanen 0437731, Arna Hyvärinen 0437621
 * 
 * Loki-ikkunaa kontrolloiva luokka
 */
package olioht;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLLogWindowController implements Initializable {
    @FXML
    private Label packageAmountLabel;
    @FXML
    private Button closeButton;
    @FXML
    private Label sentPackageAmountLabel;
    @FXML
    private TableView<Package> packageTable;
    @FXML
    private TableColumn<Package, Item> itemColumn;
    @FXML
    private TableColumn<Package, Integer> classColumn;
    @FXML
    private TableColumn<Package, Double> distanceColumn;
    @FXML
    private TableColumn<Package, String> fromAutomatColumn;
    @FXML
    private TableColumn<Package, String> toAutomatColumn;
    @FXML
    private TableColumn<Package, String> fromCityColumn;
    @FXML
    private TableColumn<Package, String> toCityColumn;
    @FXML
    private TableColumn<Package, AtomicBoolean> brokenColumn;
    @FXML
    private TableView<Package> storedPackageTable;
    @FXML
    private TableColumn<Package, Item> itemColumn1;
    @FXML
    private TableColumn<Package, String> fromAutomatColumn1;
    @FXML
    private TableColumn<Package, String> fromCityColumn1;
    @FXML
    private TableColumn<Package, String> toAutomatColumn1;
    @FXML
    private TableColumn<Package, String> toCityColumn1;
    @FXML
    private TableColumn<Package, Integer> classColumn1;

    private ObservableList<Package> sentData;
    private ObservableList<Package> storedData;
    private IntegerProperty sentPackages;
    private IntegerProperty storedPackages;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialize data structures for sent and stored packages
        sentData = LogWriter.getSentData();
        storedData = LogWriter.getStoredData();
        sentPackages = LogWriter.getSentPackages();
        storedPackages = LogWriter.getStoredPackages();
        
        // Set packageTable-table to track sentData-array
        packageTable.setItems(sentData);
        
        itemColumn.setCellValueFactory(new PropertyValueFactory<>("theItem"));
        classColumn.setCellValueFactory(new PropertyValueFactory<>("classNumber"));
        distanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
        fromAutomatColumn.setCellValueFactory(new PropertyValueFactory<>("fromAutomat"));
        fromCityColumn.setCellValueFactory(new PropertyValueFactory<>("fromCity"));
        toAutomatColumn.setCellValueFactory(new PropertyValueFactory<>("toAutomat"));
        toCityColumn.setCellValueFactory(new PropertyValueFactory<>("toCity"));
        brokenColumn.setCellValueFactory(new PropertyValueFactory<>("broken"));
        
        // Set storedPackageTable-table to track storedData-array
        storedPackageTable.setItems(storedData);
        
        itemColumn1.setCellValueFactory(new PropertyValueFactory<>("theItem"));
        classColumn1.setCellValueFactory(new PropertyValueFactory<>("classNumber"));
        fromAutomatColumn1.setCellValueFactory(new PropertyValueFactory<>("fromAutomat"));
        fromCityColumn1.setCellValueFactory(new PropertyValueFactory<>("fromCity"));
        toAutomatColumn1.setCellValueFactory(new PropertyValueFactory<>("toAutomat"));
        toCityColumn1.setCellValueFactory(new PropertyValueFactory<>("toCity"));
        
        // Set labels to track correct values
        packageAmountLabel.setText(storedPackages.getValue().toString());
        sentPackageAmountLabel.setText(sentPackages.getValue().toString());
        
        packageAmountLabel.textProperty().bind(storedPackages.asString()); 
        storedPackages.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                // value changed
            }
        });
        
        sentPackageAmountLabel.textProperty().bind(sentPackages.asString()); 
        sentPackages.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                // value changed
            }
        });
    }    

    @FXML
    private void closeAction(ActionEvent event) {
        // do whatever you want and close the window
        Node  source = (Node)  event.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
